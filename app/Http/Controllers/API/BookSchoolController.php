<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookSchoolResource;
use App\Models\BookSchool;
use App\Models\School;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BookSchoolController extends Controller
{
    public function index()
    {
        return BookSchoolResource::collection(School::with(['schoolBooks.book'])->get());
    }

    public function store(Request $request)
    {
        try {
            //validasi Setiap sekolah hanya bisa memiliki maximal 3 buku
            if (BookSchool::with('book')->where('school_id', $request->school_id)->count() + count($request->bookid) > 3) {
                return response()->json([
                    'message' => 'Tidak bisa manambahkan buku lebih dari 3 buku dalam 1 sekolah',
                ], 422);
            }

            //validasi Setiap buku hanya bisa dimiliki 2 sekolah yang berbeda
            foreach ($request->bookid as $bookid) {
                if (BookSchool::with('school')->where('book_id', $bookid)->count() >= 2) {
                    return response()->json([
                        'message' => 'Tidak bisa manambahkan buku yang dimiliki lebih dari 2 sekolah',
                    ], 422);
                }
            }

            foreach ($request->bookid as $bookid) {
                BookSchool::create([
                    'school_id' => $request->school_id,
                    'book_id' => $bookid,
                ]);
            }

            return response()->json([
                'message' => 'berasil menambahkan buku sekolah',
            ], 200);
        } catch (QueryException $e) {
            //VALIDASI UNIQUE
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                return response()->json([
                    'message' => 'Tidak bisa manambahkan buku yang sudah dimiliki',
                ], 422);
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function show($id)
    {
        return new BookSchoolResource(School::with(['schoolBooks.book'])->find($id));
    }
}

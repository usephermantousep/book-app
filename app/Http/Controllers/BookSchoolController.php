<?php

namespace App\Http\Controllers;

use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class BookSchoolController extends Controller
{
    public function index()
    {
        $request = Request::create('/api/book-school', 'GET');

        $response = Route::dispatch($request);
        $data = json_decode($response->getContent(), true)['data'];
        return view('bookschool.index', [
            'bookSchools' => $data,
        ]);
    }

    public function create()
    {
        return view('bookschool.create',[
            'schools' => School::all(),
        ]);
    }

    public function store(Request $request)
    {
        $request = Request::create('/api/book-school', 'POST', [], [], [], [], $request);

        $response = Route::dispatch($request);

        $message = json_decode($response->getContent(), true)['message'];
        if ($response->getStatusCode() != 200) {
            return redirect('book-school')->with(['error' => $message]);
        }
        return redirect('book-school')->with(['success' => $message]);
    }
}

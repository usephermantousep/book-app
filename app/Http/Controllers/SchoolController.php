<?php

namespace App\Http\Controllers;

use App\Models\School;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = Request::create('/api/school', 'GET');

        $response = Route::dispatch($request);
        $data = json_decode($response->getContent(), true)['data'];
        return view('school.index',[
            'schools' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('school.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = Request::create('/api/school', 'POST', [], [], [], [], $request);

        $response = Route::dispatch($request);

        $message = json_decode($response->getContent(), true)['message'];
        if ($response->getStatusCode() != 200) {
            return redirect('school')->with(['error' => $message]);
        }
        return redirect('school')->with(['success' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $request = Request::create('/api/school/' . $id, 'GET',);

        $response = Route::dispatch($request);

        $data = json_decode($response->getContent(), true)['data'];
        return view('school.edit', [
            'school' => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request = Request::create('/api/school/' . $id, "PUT", [], [], [], [], $request);

        $response = Route::dispatch($request);

        $message = json_decode($response->getContent(), true)['message'];
        if ($response->getStatusCode() != 200) {
            return redirect('school')->with(['error' => $message]);
        }
        return redirect('school')->with(['success' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = Request::create('/api/school/' . $id, "DELETE",);

        $response = Route::dispatch($request);

        $message = json_decode($response->getContent(), true)['message'];
        if ($response->getStatusCode() != 200) {
            return redirect('school')->with(['error' => $message]);
        }
        return redirect('school')->with(['success' => $message]);
    }
}

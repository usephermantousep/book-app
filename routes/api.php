<?php

use App\Http\Controllers\API\BookController;
use App\Http\Controllers\API\BookSchoolController;
use App\Http\Controllers\API\SchoolController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/school-book',[SchoolController::class,'index']);

Route::resource('/book',BookController::class);
Route::resource('/school',SchoolController::class);
Route::get('book-school',[BookSchoolController::class,'index']);
Route::post('book-school',[BookSchoolController::class,'store']);
Route::get('book-school/{id}',[BookSchoolController::class,'show']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookSchoolController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SchoolController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'login']);


// Route::get('/book',[BookController::class,'index']);


Route::middleware('auth')->group(
    function () {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::resource('/book', BookController::class);
        Route::resource('/school', SchoolController::class);
        Route::get('/book-school', [BookSchoolController::class, 'index']);
        Route::post('/book-school', [BookSchoolController::class, 'store']);

        Route::get('/book-school/create', [BookSchoolController::class, 'create']);
    }
);

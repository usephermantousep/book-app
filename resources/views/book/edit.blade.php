@extends('layout.main_layout')


@section('content')
    <div>
        <h1>Edit</h1>
    </div>
    <div>
        <form action="/book/{{ $book['id'] }}" method="POST">
            @method('PUT')
            @csrf
            <div class="mb-2">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name" value="{{ $book['name']}}"
                    name="name" required>
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection

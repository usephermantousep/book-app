@extends('layout.main_layout')


@section('content')
    <div>
        <h1>Add</h1>
    </div>
    <div>
        <form action="/book" method="POST">
            @csrf
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed text-nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width:10%;"><a href="#book" class="badge bg-success" id="addbook">Add
                                    <span><i class="fas fa-plus"></i></span></a></th>

                        </tr>
                    </thead>
                    <tbody id="books">
                    </tbody>
                </table>
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection

@extends('layout.main_layout')


@section('content')
    <div>
        <h1>School</h1>
    </div>
    <div>
        <form action="/school" method="POST">
            @csrf
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed text-nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width:10%;"><a href="#school" class="badge bg-success" id="addschool">Add
                                    <span><i class="fas fa-plus"></i></span></a></th>

                        </tr>
                    </thead>
                    <tbody id="schools">
                    </tbody>
                </table>
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection

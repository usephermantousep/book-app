@extends('layout.main_layout')


@section('content')
    <div>
        <h1>school</h1>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <a href="school/create" class="badge bg-success"><span><i class="far fa-plus"></i></span></a>
    </div>
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Action</th>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($schools as $school)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="d-flex">
                            <form action="/school/{{ $school['id'] }}" method="POST">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn badge bg-danger"><span><i
                                            class="far fa-times-circle"></i></span></button>
                            </form>
                            <a href="/school/{{ $school['id'] }}/edit" class="btn badge bg-warning"><span><i
                                        class="far fa-edit"></i></span></a>
                        </td>
                        <td>{{ $school['name'] }}</td>
                    </tr>
                @endforeach


            </tbody>
        </table>
    </div>
@endsection

@extends('layout.main_layout')


@section('content')
    <div>
        <h1>Edit</h1>
    </div>
    <div>
        <form action="/school/{{ $school['id']}}" method="POST">
            @method('PUT')
            @csrf
            <div class="mb-2">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name" value="{{ $school['name']}}"
                    name="name" required>
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection

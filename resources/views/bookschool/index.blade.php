@extends('layout.main_layout')


@section('content')
    <div>
        <h1>Book School</h1>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <a href="book-school/create" class="badge bg-success"><span><i class="far fa-plus"></i></span></a>
    </div>
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Total Book</th>
                    <th scope="col">Books</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bookSchools as $bookSchool)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $bookSchool['name'] }}</td>
                        <td>{{ count($bookSchool['books']) }}</td>
                        <td>
                            @foreach ($bookSchool['books'] as $books)
                            {{$books['name']}},
                            @endforeach
                        </td>
                    </tr>
                @endforeach


            </tbody>
        </table>
    </div>
@endsection

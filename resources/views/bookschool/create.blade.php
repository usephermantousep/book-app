@extends('layout.main_layout')


@section('content')
    <div>
        <h1>Add</h1>
    </div>
    <div>
        <form action="/book-school" method="POST">
            <div class="col-lg-2 mb-3">
                <label for="school_id" class="form-label">School Name</label>
                <select class="custom-select school_idselect" name="school_id" id="school_id"
                    required>
                    <option value="">--Choose School--</option>
                    @foreach ($schools as $school)
                    <option value="{{ $school->id }}">{{ $school->name }}</option>
                    @endforeach
                </select>
            </div>
            @csrf
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed text-nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width:10%;"><a href="#book" class="badge bg-success" id="addbookschool">Add
                                    <span><i class="fas fa-plus"></i></span></a></th>

                        </tr>
                    </thead>
                    <tbody id="bookschool">
                    </tbody>
                </table>
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection

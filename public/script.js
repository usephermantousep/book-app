$(document).ready(function () {
    var booki = 0;
    var schooli = 0;
    var maxBook = 3;
    var bookSchool = 0;
    $('#addbook').click(function (_) {
        $('#books').append('<tr id="rowbook' + booki + '"><td><input type"text" class="form-control col-lg-12" placeholder="Name" name="books[]"></td><td><a href="#removebook" class="badge bg-danger btn_remove" id="book' +
            booki +
            '"><span><i class="fas fa-times-circle"></i></span></a></td></tr>');
        booki++;
    });

    $('#addschool').click(function (_) {
        $('#schools').append('<tr id="rowschool' + schooli + '"><td><input type"text" class="form-control col-lg-12" placeholder="Name" name="schools[]"></td><td><a href="#removeschool" class="badge bg-danger btn_remove" id="school' +
            schooli +
            '"><span><i class="fas fa-times-circle"></i></span></a></td></tr>');
        schooli++;
    });

    $(document).on('click', '.btn_remove', function () {
        var button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();

    });

    $('#school_id').change(function (_) {
        if (bookSchool !== 0) {
            $('#bookschool').empty();
            bookSchool = 0;
        }
    })

    $('#addbookschool').click(function (_) {
        generate();
    });

    function generate() {
        $('#bookschool').append('<tr id=rowbookschool' + bookSchool + '><td><select class="custom-select my-1" name="bookid[]" id="selectbookschool' + bookSchool + '"></select></td><td><a href="#removebook" class="badge bg-danger btn_remove" id="bookschool' +
            bookSchool +
            '"><span><i class="fas fa-times-circle"></i></span></a></td></tr>');
        if (bookSchool < maxBook) {
            $.ajax({
                type: "GET",
                url: 'http://localhost:8000/api/book/',
                success: function (data) {

                    $.each(data.data, function (index, value) {
                        $('#selectbookschool' + bookSchool).append('<option value="'+value.id+'">' + value.name + '</option>');
                    });
                    bookSchool++;
                }
            });
        } else {
            alert('Tidak bisa lebih dari 3 kolom')
        }
    }
})

<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\BookSchool;


class BookSchoolSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookSchool::insert([
            [
                'school_id' => 1,
                'book_id' => 2,
            ],
            [
                'school_id' => 1,
                'book_id' => 3,
            ],
            [
                'school_id' => 2,
                'book_id' => 1,
            ],
            [
                'school_id' => 2,
                'book_id' => 2,
            ],
            [
                'school_id' => 3,
                'book_id' => 1,
            ],
            [
                'school_id' => 3,
                'book_id' => 3,
            ],
        ]);
    }
}

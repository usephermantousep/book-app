<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\School;


class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::insert([
            [
                'name' => 'SMA N 1 ABC',
            ],
            [
                'name' => 'SMA N 2 ABC',
            ],
            [
                'name' => 'SMA N 3 ABC',
            ]
        ]);
    }
}

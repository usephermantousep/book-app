<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_schools', function (Blueprint $table) {
            $table->id();
            $table->foreignId('book_id');
            $table->foreignId('school_id');
            $table->timestamps();
            $table->unique(['book_id','school_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_schools');
    }
};
